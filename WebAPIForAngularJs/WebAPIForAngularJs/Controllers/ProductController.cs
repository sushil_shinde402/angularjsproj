﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPIForAngularJs.Models;

namespace WebAPIForAngularJs.Controllers
{
    public class ProductController : ApiController
    {
        public static Lazy<List<Product>> products = new Lazy<List<Product>>();
        public static int PageLoadFlag = 1;
        public static int ProductID = 4;

        public ProductController()
        {
            if (PageLoadFlag == 1) //use this only for first time page load
            {
                //Three product added to display the data
                products.Value.Add(new Product { ID = 1, Name = "bus", Category = "Toy", Price = 200.12M });
                products.Value.Add(new Product { ID = 2, Name = "Car", Category = "Toy", Price = 300 });
                products.Value.Add(new Product { ID = 3, Name = "robot", Category = "Toy", Price = 3000 });
                PageLoadFlag++;
            }
        }

        public List<Product> GetAllProducts()
        {
            return products.Value;
        }
        public IHttpActionResult GetProduct(int id)
        {
            Product product = products.Value.FirstOrDefault((p) => p.ID == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }

        public void ProductAdd(Product product) //post method
        {
            product.ID = ProductID;
            products.Value.Add(product); //add the post product data to the product list
            ProductID++;
            //instead of adding product data to the static product list you can save data to the database.
        }
    }
}
